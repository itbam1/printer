package bt.com.itbam.devices.printers;

import android.graphics.Bitmap;

class PrinterUtils {

    static Bitmap bitmapToBlackWhite(final Bitmap bitmap) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Bitmap bmOut = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        final float factor = 255f;
        final float redBri = 0.2126f;
        final float greenBri = 0.2126f;
        final float blueBri = 0.0722f;

        int length = width * height;
        int[] inpixels = new int[length];
        int[] oupixels = new int[length];

        bitmap.getPixels(inpixels, 0, width, 0, 0, width, height);

        int point = 0;
        for (int pix : inpixels) {
            int R = (pix >> 16) & 0xFF;
            int G = (pix >> 8) & 0xFF;
            int B = pix & 0xFF;

            float lum = (redBri * R / factor) + (greenBri * G / factor) + (blueBri * B / factor);

            if (lum > 0.4) {
                oupixels[point] = 0xFFFFFFFF;
            } else {
                oupixels[point] = 0xFF000000;
            }
            point++;
        }

        bmOut.setPixels(oupixels, 0, width, 0, 0, width, height);
        return bmOut;
    }

    static int px2Byte(int pixel) {
        int red = (pixel & 16711680) >> 16;
        int green = (pixel & '\uff00') >> 8;
        int blue = pixel & 255;
        int gray = RGB2Gray(red, green, blue);
        byte b;
        if (gray < 127) {
            b = 1;
        } else {
            b = 0;
        }

        return b;
    }

    static int RGB2Gray(int r, int g, int b) {
        return (int)(0.299D * (double)r + 0.587D * (double)g + 0.114D * (double)b);
    }
}
