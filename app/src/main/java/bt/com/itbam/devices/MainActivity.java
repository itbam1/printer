package bt.com.itbam.devices;
import android.graphics.Matrix;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import bt.com.itbam.devices.printers.Printer;
import bt.com.itbam.devices.printers.PrinterDevice;
import bt.com.itbam.devices.printers.PrinterDeviceListener;
import bt.com.itbam.devices.printers.PrinterFactory;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private UsbManager manager;
    private Printer printer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.list_view);
        listView.setAdapter(new CurrentAdapter(this.getApplicationContext()));
    }

    public void searchDevices(View view) {
        printer = PrinterFactory.Companion.getPrinter(this.getApplicationContext(), "Printer");
    }

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Log.d(TAG, "permission for device " + device);
                        if(device != null){
                            //call method to set up device communication
                        }
                    }
                    else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    };
    public void connectToPrint(final View view) {

    }

    public void testPrint(final View view ) {
        if (printer != null) {
//            printer.setStandardMode();
            printer.initialize();
//            printer.getStatus();
//            printer.setUpsideDownPrintingMode(true);
//            printer.selectFont(PrinterDevice.FONT_TYPE.FONT_A);
//            // printer.configurePrintMode(true, false, false, false, false);
//            printer.setCharacterSpace(20);
//            printer.print("Before Set Character Space 10");
//            printer.lineFeed();
//            printer.setCharacterSpaceDefault();
//            printer.print("Before Set Character Space Default");
//            printer.setUnderlineMode(PrinterDevice.UNDERLINE_TYPE.ONE_DOT);
//            printer.print("before Set underline mode one dot");
//            printer.lineFeed();
//            printer.setLineSpace(50);
//            printer.print("1x before Set Line Space = 50");
//            printer.lineFeed();
//            printer.setLineSpace(100);
//            printer.print("2x before Set Line Space = 100");
//            printer.lineFeed();
//            printer.setUnderlineMode(PrinterDevice.UNDERLINE_TYPE.TWO_DOTS);
//            printer.print("before Set underline mode two dots");
//            printer.setDefaultLineSpace();
//            printer.print("1x before Set Default Line Space");
//            printer.lineFeed();
//            printer.setJustification(PrinterDevice.JUSTIFICATION_TYPE.RIGHT);
//            printer.print("2x before Set Default Line Space and Justification Right");
//            printer.lineFeed();
//            printer.setPrintDirection(1);
//            printer.lineFeed();
//            printer.setUnderlineMode(PrinterDevice.UNDERLINE_TYPE.DISABLE);
//            printer.print("Before disabled underline");
//            printer.setEmphasized(true);
//            printer.print("Set emphasized mode True");
//            printer.lineFeed();
//            printer.setEmphasized(false);
//            printer.print("Set emphasized mode False");
//            printer.lineFeed();
//            printer.setHeightDefaultBarCode();

//            for (PrinterDevice.CHARACTER_SET_TYPE type :PrinterDevice.CHARACTER_SET_TYPE.values()) {
//                printer.setCharset(i);
//                printer.print("a â á à ã e é è ê i ì í î o ò ó õ ô u ú ù û ç");
//                printer.lineFeed();


            for (int i = 0; i < 25; i++) {
                printer.setCharset(i);
                printer.printChar();
            }

//            for (int i = 0; i <= 255; i++) {
//                printer.print("Set Character Code Table : " + i);
//                printer.lineFeed();
//                android.util.Log.e("MainActivity", "ret: " + printer.setCharset(i));
//                printer.print("A Â Á À Ã R É È Ê I Í Ì O Ó Õ Ô U Ú Ù Ç ");
//                printer.lineFeed();
//            }



//
            for (int i = 16; i <= 19; i++) {
                android.util.Log.e("MainActivity", "ret: " + printer.setCharacterCodeTable(i));
                printer.printChar();
            }
//
//            printer.print("Set Character Code Table : 255");
//            printer.lineFeed();
//            android.util.Log.e("MainActivity", "ret: " + printer.setCharset(PrinterDevice.CHARACTER_SET_TYPE.LATIN_AMERICA));
//            printer.print("A Â Á À Ã R É È Ê I Í Ì O Ó Õ Ô U Ú Ù Ç ");
//            printer.lineFeed();
//
//            for (int i = 0; i < 15; i++) {
//                printer.setCharset(i);
//                android.util.Log.e("MainActivity", "ret: " + printer.print("A Â Á À Ã R É È Ê I Í Ì O Ó Õ Ô U Ú Ù Ç "));
//                printer.lineFeed();
//            }
//
//            printer.lineFeed(2);
//
//            /** Teste UPC-A **/
//            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//            int b = 48;
//            String barCode = "123456789012";
//            printer.print("Print BarCode Set");
//            printer.setHeightBarCode(10);
//            printer.print("Print BarCode UPC_A length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.UPC_A, barCode);
//
//            printer.print("Print BarCode UPC_A_2 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.UPC_A_2, barCode);
//
//            barCode = "012345678901";
//            printer.print("Print BarCode UPC_E length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.UPC_E, barCode);
//
//            printer.print("Print BarCode UPC_E_2 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.UPC_E_2, barCode);
//
//            barCode = "1234567890123";
//            printer.print("Print BarCode EAN13 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.EAN13, barCode);
//
//            printer.print("Print BarCode EAN13_2 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.EAN13_2, barCode);
//
//            barCode ="01234567";
//            printer.print("Print BarCode EAN8 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.EAN8, barCode);
//
//            printer.print("Print BarCode EAN8_2 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.EAN8_2, barCode);
//
//            // TODO: Verificar
////            barCode = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ$%+-";
////            printer.print("Print BarCode CODE39 length(" + barCode.length() + ")");
////            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.CODE39, barCode);
////            outputStream.reset();
//
//            printer.print("Print BarCode CODE39_2 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.CODE39_2, barCode);
//            outputStream.reset();
//
//            barCode = "012345678901234567890";
//            printer.print("Print BarCode ITF length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.ITF, barCode);
//
//            printer.print("Print BarCode ITF_2 length(" + barCode.length() + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.ITF_2, barCode);
//
//            // CODABAR -- TODO: Verificar CODABAR
//            outputStream.reset();
//            for (int x = 0; x < 2; x++) {
//                for (b = 48; b <= 57; b++) {
//                    outputStream.write((byte) b++);
//                }
//                for (b = 65; b <= 68; b++) {
//                    outputStream.write((byte) b++);
//                }
//            }
//            barCode = "0123456789ABCD0123456789ABCD";
//            printer.print("Print BarCode CODABAR length(" + outputStream.toByteArray().length + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.CODABAR, outputStream.toByteArray());
//
//            printer.print("Print BarCode CODABAR length(" + outputStream.toByteArray().length + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.CODABAR_2, outputStream.toByteArray());
//
//            barCode = "01234567890123456789ABCDEFGHIJKLMNOPQRST+1-*/.";
//            printer.print("Print BarCode CODE93 length(" + outputStream.toByteArray().length + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.CODE93, outputStream.toByteArray());
//
//            printer.print("Print BarCode CODE128 length(" + outputStream.toByteArray().length + ")");
//            printer.printerBarCodeCommand(PrinterDevice.BARCODE_TYPE.CODE128, outputStream.toByteArray());
//
//
//            printer.lineFeed();
//            printer.print("Imprimindo meu Qr Code MODEL ONE");
//            printer.lineFeed();
//
//            printer.setQrCodeModel(PrinterDevice.QR_CODE_MODEL.MODEL_ONE);
//            printer.setQrCodeSize(180);
//            printer.setQrCodeErrorCoorrectionLevel(PrinterDevice.QR_CODE_LEVEL.LEVEL_H);
//            printer.printQrCodeStorageData(false, "123456789-OLA MUNDO 123456789");
//            printer.lineFeed(2);
//
//            printer.lineFeed();
//            printer.setQrCodeModel(PrinterDevice.QR_CODE_MODEL.MODEL_ONE);
//            printer.setQrCodeSize(500);
//            printer.setQrCodeErrorCoorrectionLevel(PrinterDevice.QR_CODE_LEVEL.LEVEL_H);
//            printer.printQrCodeStorageData(false, "123456789-OLA MUNDO 123456789");
//
//            printer.lineFeed();
//            printer.print("Imprimindo meu Qr Code MODEL TWO");
//            printer.lineFeed();
//            printer.setQrCodeModel(PrinterDevice.QR_CODE_MODEL.MODEL_TWO);
//            printer.setQrCodeSize(180);
//            printer.setQrCodeErrorCoorrectionLevel(PrinterDevice.QR_CODE_LEVEL.LEVEL_L);
//            printer.printQrCodeStorageData(false, "123456789-OLA MUNDO 123456789");
//            printer.lineFeed(2);
//
//            printer.lineFeed();
//            printer.setQrCodeModel(PrinterDevice.QR_CODE_MODEL.MODEL_TWO);
//            printer.setQrCodeSize(500);
//            printer.setQrCodeErrorCoorrectionLevel(PrinterDevice.QR_CODE_LEVEL.LEVEL_L);
//            printer.printQrCodeStorageData(false, "123456789-OLA MUNDO 123456789");
//            printer.lineFeed(4);



//            ImageView imageView = new ImageView(this);
//            imageView.setImageResource(R.mipmap.ic_launcher);
//            BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
//            Bitmap bitmap = bitmapDrawable.getBitmap();

//            View bView = this.findViewById(R.id.main_layout);

//            Bitmap bitmap = Bitmap.createBitmap(bView.getWidth(), bView.getHeight(), Bitmap.Config.ARGB_8888);
//            Bitmap bitMap = getResizedBitmap(bitmap, 500, 500);
//            printer.print(bitMap);
//            printer.lineFeed(5);
//            printer.print("Imprimindo meu Qr Code MODEL ONE");
//            printer.printQrCodeStorageData("Imprimindo meu Qr Code MODEL ONE");
//            printer.lineFeed(10);
//            printer.sendCut();
//
//            printer.cancel();
        }
    }


    public Bitmap getResizedBitmap(final Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
}
