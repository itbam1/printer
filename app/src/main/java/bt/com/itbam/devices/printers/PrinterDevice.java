package bt.com.itbam.devices.printers;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import bt.com.itbam.devices.builder.CommandBuilder;

public class PrinterDevice implements Printer {

    /** Start Command Printer List */
    private static final byte[] COMMAND_GET_STATUS = {0x10, 0x04, 0x01, 0x00}; // GET STATUS COMMAND
    private static final byte[] COMMAND_INITIALIZE = { 0x1B, 0x40 }; // COMMAND_LF : Line Feed Command - Funcionando
    private static final byte[] COMMAND_HORIZONTAL_TAB = { 0x09 }; // HORIZONTAL TAB : Horizontal Tabulation
    private static final byte[] COMMAND_LF = { 0x0A }; // COMMAND_LF : Line Feed Command - Funcionando
    private static final byte[] COMMAND_LF_N = { 0x1B, 0x64 }; // COMMAND_LF_N : Print and feed n lines - Funcionando
    private static final byte[] COMMAND_SELECT_FONT = {0x1B, 0x4D}; // GS(1D) f(66) : Select font for Human Readlable Interpretation (HRI)characters
    private static final byte[] COMMAND_UNDERLINE_MODE = {0x1B, 0x2D}; // GS(1D) f(66) : Select font for Human Readlable Interpretation (HRI)characters
    private static final byte[] COMMAND_JUSTIFICATION_MODE = {0x1B, 0x61}; // GS(1B) f(61) : Select justification mode
    private static final byte[] COMMAND_TURN_UPSIDE_DOWN_PRINTING_MODE = {0x1B, 0x7B}; // ESC(27) {(123) : Select justification mode
    private static final byte[] COMMAND_SET_DEFAULT_LINE_SPACING = {0x1B, 0x32}; // ESC(1B/27) 2(32/50) Select default line space mode
    private static final byte[] COMMAND_SET_LINE_SPACING  = {0x1B, 0x33}; // ESC(1B/27) 2(32/50) Select default line space mode
    private static final byte[] COMMAND_PRINT_DIRECTION  = {0x1B, 0x54}; // ESC(1B/27) T(54/84) Select Print Direction In Page Mode
//    private static final byte[] COMMAND_CUT_PAPER = { 0x1D, 0x56, 0x31 }; // ESC i : Cut Paper Command
    private static final byte[] COMMAND_CUT_PAPER = { 0x1D, 0x56, 1, 66 }; // ESC i : Cut Paper Command

//    private static final byte[] COMMAND_CUT_PAPER = { 0x1B, 0x69 };
    private static final byte[] COMMAND_CUT_PAPER_CH = { 29, 86, 49 }; // ESC m : Cut Paper Command - Funcionando
    private static final byte[] COMMAND_CUT_PAPER_PARTIAL = { 0x1B, 0x6D }; // ESC m : Cut Paper Command
    private static final byte[] COMMAND_BARCODE_HEADER = { 0x1D, 0x6B }; // GS k : Print Barcode Header
    private static final byte[] COMMAND_SET_WIDTH_BARCODE = { 0x1D, 0x77 }; // GS w : Set Bar Code Width
    private static final byte[] COMMAND_PRINT_IMAGE = {0x1D, 0x76, 0x30}; // GS(1D) v(76) 0(30) m xL xH yL yH
    private static final byte[] COMMAND_CR = { 0x0D };
    private static final byte[] COMMAND_STANDARD_MODE = { 0x0C }; // FF = 12 : Print and return to standard mode in page mode
    private static final byte[] COMMAND_PAGE_MODE = { 0x1B, 0x0C }; // FF = 12 : Print and return to standard mode in page mode
    private static final byte[] COMMAND_CONFIG_PRINT_MODE = { 0x1B, 0x21 }; // 1B = 21 : Select Print Mode
    private static final byte[] COMMAND_CANCEL = { 0x18 }; // Cancel print data in page mode
    private static final byte[] COMMAND_CHARACTER_SPACE = {0x1B, 0x20};
    private static final byte[] COMMAND_EMPHASIZED_MODE = {0x1B, 0x45};
    private static final byte[] COMMAND_DOUBLE_STRIKE_MODE = {0x1B, 0x47};
    private static final byte[] COMMAND_SET_ABSOLUTE_PRINT_POSITION = {0x1B, 0x24};

    /** COMMANDS QR CODE */
    //                                                               GS    (      k    pL     pH   cn    fn    n1    n2
    private static final byte[] COMMAND_SET_QR_CODE_MODEL_SELECT = {0x1D, 0x28, 0x6B, 0x04, 0x00, 0x31, 0x41, 0x00, 0x00}; // Select The QR CODE Model One
    private static final byte[] COMMAND_SET_QR_CODE_MODEL_ONE = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x31, 0x00}; // Select The QR CODE Model One
    private static final byte[] COMMAND_SET_QR_CODE_MODEL_TWO = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x32, 0x00}; // Select The QR CODE Model Two
    private static final byte[] COMMAND_SET_QR_CODE_SIZE = {0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x43, 0x03}; // Set The QR Code Size Of Module
    private static final byte[] COMMAND_SET_QR_CODE_ERROR_CORRECTION_LEVEL = {0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x45, 0x00}; // Select the QR Code error correction level (lastBit: 47 < n < 52)
    private static final byte[] COMMAND_STORE_QR_CODE_HEADER_DATA = {0x1D, 0x28, 0x6B, 0x00, 0x00, 0x31, 0x50, 0x30}; // Store the QR Code data in the symbol storage area
    private static final byte[] COMMAND_STORE_QR_CODE_PRINT_DATA = {0x1D, 0x28, 0x6B, 0x03, 0x00, 0x31, 0x51, 0x30}; // Store the QR Code data in the symbol storage area

    // set charset
    private static final byte[] COMMAND_CHARSET = {0x1B, 0x52/*, n */}; // 27, 82, 0 <= n <= 13
    private static final byte[] COMMAND_CHARACTER_CODE_TABLE = {0x1B, 0x74/*, n */}; // 27, 116, 0 <= n <= 5 || n=17 || n=255

    private static final int CHARACTER_SPACE_DEFAULT = 0;
    private UsbEndpoint endPointIn;

    /** End Command Printer List */

    public enum FONT_TYPE {
        FONT_A(0x48), FONT_B(0x49);

        final int type;
        FONT_TYPE(final int type) {
            this.type = type;
        }
    }

    public enum BARCODE_TYPE {
        UPC_A(0), UPC_E(1), EAN13(2), EAN8(3),
        CODE39(4), ITF(5), CODABAR(6), UPC_A_2(65),
        UPC_E_2(66), EAN13_2(67), EAN8_2(68), CODE39_2(69),
        ITF_2(70), CODABAR_2(71), CODE93(72), CODE128(73);
        final int type;
        BARCODE_TYPE(final int i) {
            type = i;
        }
    }

    public enum BARCODE_WIDTH_TYPE {
        WIDTH_ONE(2), WIDTH_TWO(3), WIDTH_THREE(4),
        WIDTH_FOUR(5), WIDTH_FIVE(6), WIDTH_SIX(7);

        final int type;
        BARCODE_WIDTH_TYPE(final int i) {
            type = i;
        }
    }

    public enum UNDERLINE_TYPE {
        DISABLE(0), ONE_DOT(1), TWO_DOTS(2);

        private final int type;
        UNDERLINE_TYPE(final int i) {
            type = i;
        }
    }

    public enum JUSTIFICATION_TYPE {
        LEFT(0), ONE_DOT(1), RIGHT(2);

        private final int type;
        JUSTIFICATION_TYPE(final int i) {
            type = i;
        }
    }

    public enum QR_CODE_MODEL {
        MODEL_ONE(49), MODEL_TWO( 50 );

        private final int type;
        QR_CODE_MODEL(final int i) {
            type = i;
        }
    }

    public enum QR_CODE_LEVEL {
        LEVEL_L(0x30), LEVEL_M(0x31),
        LEVEL_Q(0x32), LEVEL_H(0x33);

        private final int type;
        QR_CODE_LEVEL(final int i) {
            type = i;
        }
    }

    public enum CHARACTER_SET_TYPE {
        USA(0), FRANCE(1), GERMANY(2), UK(3),
                DENMARK_I(4), SWEDEN(5), ITALY(6), SPAIN_I(7),
                JAPAN(8), NORWAY(9), DENMARK_II(10),
                SPAIN_II(11), LATIN_AMERICA(12);

        private final int cod;

        CHARACTER_SET_TYPE(int i) {
            this.cod = i;
        }
    }

    public static final int HEIGHT_BARCODE_DEFAULT = 162;
    private static final int TIMEOUT_DEFAULT = 2000;
    private static final String PRINTER_DEVICE_TAG = "Printer Device";

    private final UsbDeviceConnection deviceConnection;
    private UsbEndpoint endPointOut;

    private int timeout = TIMEOUT_DEFAULT;


    public PrinterDevice(UsbDeviceConnection deviceConnection) {
        this.deviceConnection = deviceConnection;
    }

    protected PrinterDevice(final UsbDeviceConnection deviceConnection, final UsbEndpoint endPointOut, final UsbEndpoint endPointIn) {
        this.deviceConnection = deviceConnection;
        this.endPointOut = endPointOut;
        this.endPointIn = endPointIn;
    }

    public void setEndPointOut(UsbEndpoint endPointOut) {
        this.endPointOut = endPointOut;
    }

    public UsbEndpoint getEndPointOut() {
        return endPointOut;
    }

    public void setEndPointIn(final UsbEndpoint endPointIn) {
        this.endPointIn = endPointIn;
    }

    public UsbEndpoint getEndPointIn() {
        return endPointIn;
    }

    /** {@inheritDoc} */
    @Override
    public int initialize() {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            Log.d(PRINTER_DEVICE_TAG, "Send Initialize Printer Command ...");
            result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_INITIALIZE, COMMAND_INITIALIZE.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Initialize Printer Command: " + result);
        }
        return result;
    }

    @Override
    public String getStatus() {

        Log.d(PRINTER_DEVICE_TAG, "Get Status Command ${COMMAND_GET_STATUS}");
        byte[] bCommand = COMMAND_GET_STATUS;

        int result = deviceConnection.bulkTransfer(this.endPointOut, bCommand, bCommand.length, timeout);

        Log.d(PRINTER_DEVICE_TAG, "Result: Get Status Command: ");

        if (result > 0) {
            for (byte b : bCommand) {
                Log.e(PRINTER_DEVICE_TAG, "Result: " + b);
            }
            byte[] bResult  = new byte[2];
            int size = 1;
            result = deviceConnection.bulkTransfer(this.endPointIn, bResult, size, timeout);

            if (result > 0) {
                Log.d(PRINTER_DEVICE_TAG, "Result: " + result);
                Log.d(PRINTER_DEVICE_TAG, "Result length: " + bResult.length);
            }
        }

        return bCommand.toString();
    }
    @Override
    public int horizontalTab() {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            Log.d(PRINTER_DEVICE_TAG, "Horizontal Tab Command...");
            result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_HORIZONTAL_TAB, COMMAND_HORIZONTAL_TAB.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Line Feed Command: " + result);
        }
        return result;
    }

    @Override
    public int lineFeed() {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            Log.d(PRINTER_DEVICE_TAG, "Send Line Feed Command...");
            result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_LF, COMMAND_LF.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Line Feed Command: " + result);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int lineFeed(int lines) {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int result = -1;
        try {
            if (endPointOut == null) {
                Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
            } else {
                outputStream.write(COMMAND_LF_N);
                outputStream.write(lines);
                Log.d(PRINTER_DEVICE_TAG, "Send N-Line Feed Command...");
                result = deviceConnection.bulkTransfer(this.endPointOut, outputStream.toByteArray(), outputStream.size(), getTimeout());
                Log.d(PRINTER_DEVICE_TAG, "Result N-Line Feed Command: " + result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int carriageReturn() {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int result = -1;
        try {

            if (endPointOut == null) {
                Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
            } else {
                outputStream.write(COMMAND_CR);
                Log.d(PRINTER_DEVICE_TAG, "Send N-Line Feed Command...");
                result = deviceConnection.bulkTransfer(this.endPointOut, outputStream.toByteArray(), outputStream.size(), getTimeout());
                Log.d(PRINTER_DEVICE_TAG, "Result N-Line Feed Command: " + result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setStandardMode() {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            Log.d(PRINTER_DEVICE_TAG, "Send Standard Mode Command...");
            result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_STANDARD_MODE, COMMAND_STANDARD_MODE.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Standard Mode Command: " + result);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setPageMode() {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            Log.d(PRINTER_DEVICE_TAG, "Send Standard Mode Command...");
            result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_PAGE_MODE, COMMAND_PAGE_MODE.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Standard Mode Command: " + result);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int configurePrintMode(boolean changeFont, boolean emphasized, boolean doubleHeight, boolean doubleWidth, boolean underline) {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            int iCommand = underline ? 1 : 0; // 0
            iCommand <<= 2;
            iCommand |= doubleWidth ? 1 : 0; // 3
            iCommand <<= 1;
            iCommand |= doubleHeight ? 1 : 0; // 4
            iCommand <<= 1;
            iCommand |= emphasized ? 1 : 0; // 5
            iCommand <<= 3;
            iCommand |= changeFont ? 1 : 0; // 7

            byte[] abCommand = CommandBuilder.builder()
                    .addCommand(COMMAND_CONFIG_PRINT_MODE)
                    .addCommand(iCommand)
                    .build();


            Log.d(PRINTER_DEVICE_TAG, "Send Configure Mode Command...: " + iCommand);

            //COMMAND_CONFIG_PRINT_MODE
            result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Configure Mode Command: " + result);
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setCharacterSpaceDefault() {
        return setCharacterSpace(CHARACTER_SPACE_DEFAULT);
    }

    /** {@inheritDoc} */
    @Override
    public int setCharacterSpace(int spaceValue) {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            if (spaceValue < 0 || spaceValue > 255) {
                Log.d(PRINTER_DEVICE_TAG, "Send Spacing Character Command Error The Value not inner 0 and 255 : ( " + spaceValue + ")");
            } else {
                byte[] abCommand = CommandBuilder.builder()
                        .addCommand(COMMAND_CHARACTER_SPACE)
                        .addCommand(spaceValue)
                        .build();
                Log.d(PRINTER_DEVICE_TAG, "Send Spacing Character Command (" + spaceValue + ")... ");
                result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
                Log.d(PRINTER_DEVICE_TAG, "Result Spacing Character Command: " + result);
            }
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setEmphasized(final boolean emphasized) {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {

            byte[] abCommand = CommandBuilder.builder()
                    .addCommand(COMMAND_EMPHASIZED_MODE)
                    .addCommand(emphasized ? 0x01 : 0x00)
                    .build();
            Log.d(PRINTER_DEVICE_TAG, "Send Emphasized Command (" + emphasized+ ")... ");
            result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Emphasized Command: " + result);
        }

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public int setDoubleStrike(final boolean doubleStrike) {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {

            byte[] abCommand = CommandBuilder.builder()
                    .addCommand(COMMAND_DOUBLE_STRIKE_MODE)
                    .addCommand(doubleStrike ? 0x01 : 0x00)
                    .build();

            Log.d(PRINTER_DEVICE_TAG, "Send Double Strike Command (" + doubleStrike+ ")... ");
            result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Double Strike Command: " + result);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setAbsolutePrintPosition(int posX, int posY) {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            if (posY < 0 || posY > 255 || posX < 0 || posX > 255) {
                Log.d(PRINTER_DEVICE_TAG, "Send Set Absolute Printer Position Error The Value not inner 0 and 255");
            } else {
                byte[] abCommand = CommandBuilder.builder()
                        .addCommand(COMMAND_SET_ABSOLUTE_PRINT_POSITION)
                        .addCommand(posX)
                        .addCommand(posY)
                        .build();
                Log.d(PRINTER_DEVICE_TAG, "Send Set Absolute Printer Position Command (" + posX + ", " + posY + ")... ");
                result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
                Log.d(PRINTER_DEVICE_TAG, "Result Set Absolute Printer Position Command: " + result);
            }
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int cancel() {
        int result = -1;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        } else {
            Log.d(PRINTER_DEVICE_TAG, "Send Cancel Command...");
            result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_CANCEL, COMMAND_CANCEL.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Cancel Command: " + result);
        }

        return result;
    }

    @Override
    public void sendCut() {
        int result = 0;
        if (endPointOut == null) {
            Log.d(PRINTER_DEVICE_TAG, "End Point from Printer device is null");
        }


        Log.d(PRINTER_DEVICE_TAG, "Send Cut Command...");
//        result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_CUT_PAPER, COMMAND_CUT_PAPER.length, getTimeout()); // BUGA
        Log.d(PRINTER_DEVICE_TAG, "Result Cut Command 1: " + result);
//        result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_CUT_PAPER_CH, COMMAND_CUT_PAPER_CH.length, getTimeout());
        Log.d(PRINTER_DEVICE_TAG, "Result Cut Command 2: " + result);
        result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_CUT_PAPER, COMMAND_CUT_PAPER.length, getTimeout());
        Log.d(PRINTER_DEVICE_TAG, "Result Cut Command 3: " + result);
//        result = deviceConnection.bulkTransfer(this.endPointOut, COMMAND_CUT_PAPER_PARTIAL, COMMAND_CUT_PAPER_PARTIAL.length, getTimeout());
        Log.d(PRINTER_DEVICE_TAG, "Result Cut Command 4: " + result);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int print(final String buffer) {
        int result = 0;
        if (endPointOut != null) {



            Log.d(PRINTER_DEVICE_TAG, "Send Print Command (" + buffer + ") ...");

            result = deviceConnection.bulkTransfer(this.endPointOut, buffer.getBytes(), buffer.getBytes().length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Print Command: " + result);
        }

        return result;
    }

    @Override
    public int printChar() {
        int result = 0;
        if (endPointOut != null) {
            byte buffer[] = {(byte)0xB5}; // Á
            Log.d(PRINTER_DEVICE_TAG, "Send Print Command (" + buffer + ") ...");

            result = deviceConnection.bulkTransfer(this.endPointOut, buffer, buffer.length, getTimeout());
            Log.d(PRINTER_DEVICE_TAG, "Result Print Command: " + result);
        }

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int selectFont(final FONT_TYPE fontType) {
        Log.e("PDV-PINPAD", "Send Select Font : \tfont(" + fontType.type + ")");
        final byte[] builder = CommandBuilder.builder()
                .addCommand(COMMAND_SELECT_FONT)
                .addCommand(fontType.type)
                .build();

        int iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());
        Log.e("PDV-PINPAD", "Send Select Font: \tresult: (" + iResult + ")");

        return iResult;
    }

    /** {@inheritDoc} */
    @Override
    public int setUnderlineModeDefault() {
        return setUnderlineMode(UNDERLINE_TYPE.ONE_DOT);
    }

    /**{@inheritDoc}*/
    @Override
    public int setUnderlineMode(final UNDERLINE_TYPE dotsMode) {
        Log.e("PDV-PINPAD", "Send Underline Mode : \t (" + dotsMode.type + ")");
        final byte[] builder = CommandBuilder.builder()
                .addCommand(COMMAND_UNDERLINE_MODE)
                .addCommand(dotsMode.type)
                .build();

        int iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());
        Log.e("PDV-PINPAD", "Send Select Font: \tresult: (" + iResult + ")");

        return iResult;
    }

    /**{@inheritDoc}*/
    @Override
    public int setJustification(final JUSTIFICATION_TYPE justificationType) {
        Log.e("PDV-PINPAD", "Send Justification type:");
        int iResult = -1;
        if (endPointOut != null) {
            final byte[] builder = CommandBuilder.builder()
                    .addCommand(COMMAND_JUSTIFICATION_MODE)
                    .addCommand(justificationType.type)
                    .build();

            iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());

            Log.e("PDV-PINPAD", "Send Default Line Space: \tresult: (" + iResult + ")");
        }

        return iResult;
    }

    /**{@inheritDoc}*/
    @Override
    public int setUpsideDownPrintingMode(final boolean enabled) {
        Log.e("PDV-PINPAD", "Send Justification type:");
        int iResult = -1;
        if (endPointOut != null) {
            final byte[] builder = CommandBuilder.builder()
                    .addCommand(COMMAND_TURN_UPSIDE_DOWN_PRINTING_MODE)
                    .addCommand(enabled ? 1 : 0)
                    .build();

            iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());

            Log.e("PDV-PINPAD", "Send Default Line Space: \tresult: (" + iResult + ")");
        }

        return iResult;
    }

    /** {@inheritDoc} */
    @Override
    public int setDefaultLineSpace() {
        Log.e("PDV-PINPAD", "Send Default Line Space :");

        final byte[] builder = CommandBuilder.builder()
                .addCommand(COMMAND_SET_DEFAULT_LINE_SPACING)
                .build();

        int iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());

        Log.e("PDV-PINPAD", "Send Default Line Space: \tresult: (" + iResult + ")");

        return iResult;
    }

    /** {@inheritDoc} */
    @Override
    public int setLineSpace(final int lineSpace) {
        Log.e("PDV-PINPAD", "Send Default Line Space :");
        int iResult = -1;
        if (lineSpace < 0 || lineSpace > 255) {
            Log.e("PDV-PINPAD", "Send Default Line Space : Error : value (0 <= x <= 255");
        } else {
            final byte[] builder = CommandBuilder.builder()
                    .addCommand(COMMAND_SET_LINE_SPACING)
                    .addCommand(lineSpace)
                    .build();
            iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());
        }

        Log.e("PDV-PINPAD", "Send Default Line Space: \tresult: (" + iResult + ")");
        return iResult;
    }

    /** {@inheritDoc} */
    @Override
    public int setPrintDirection(final int pageMode) {
        Log.e("PDV-PINPAD", "Send Print Direction :");
        int iResult = -1;
        if (pageMode < 0 || pageMode > 3) {
            Log.e("PDV-PINPAD", "Send Print Direction Error ");
        } else {
            final byte[] builder = CommandBuilder.builder()
                    .addCommand(COMMAND_PRINT_DIRECTION)
                    .addCommand(pageMode)
                    .build();
            iResult = deviceConnection.bulkTransfer(this.endPointOut, builder, builder.length, getTimeout());
        }

        Log.e("PDV-PINPAD", "Send Default Line Space: \tresult: (" + iResult + ")");
        return iResult;
    }
    public static Bitmap getBitmapForMutable(Bitmap bitmap) {
        Bitmap result = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint(6);
        canvas.drawBitmap(bitmap, 0.0F, 0.0F, paint);
        return result;
    }

    private static void setPixel(Bitmap reSizeBitmap, int width, int realHeight, int height) {
        for(int j = 0; j < width; ++j) {
            for(int i = 0; i < height; ++i) {
                if (i < realHeight) {
                    int color = reSizeBitmap.getPixel(j, i);
                    int r = Color.red(color);
                    int g = Color.green(color);
                    int b = Color.blue(color);
                    r = (9798 * r + 19235 * g + 3735 * b) / '耀';
                    int oldcolor = 0;
                    if (r > 127) {
                        oldcolor = Color.rgb(255, 255, 255);
                    } else {
                        oldcolor = Color.rgb(0, 0, 0);
                    }

                    reSizeBitmap.setPixel(j, i, oldcolor);
                }
            }
        }

    }


    public byte[] printImageForPin(Bitmap bitmap) {
        ArrayList<Byte> data = new ArrayList();
        Bitmap reSizeBitmap = getBitmapForMutable(bitmap);
        int width = reSizeBitmap.getWidth();
        int realHeight = reSizeBitmap.getHeight();
        int height = (realHeight + 7) / 8 * 8;
        if (width > 420) {
            width = 420;
        }

        byte[] buf = new byte[]{27, 42, 1, (byte)(width % 256), (byte)(width / 256)};
        byte[] lineFeed = new byte[]{27, 74, 15};
        byte[] lineFeed2 = new byte[]{27, 74, 1};
        byte[] imageCommand = new byte[]{27, 85, 1, 27, 42, 1, (byte)(width % 256), (byte)(width / 256)};
        int position = 0;
        setPixel(reSizeBitmap, width, realHeight, height);

        for(int i = 0; i < height; i += 16) {
            byte[] cmdData1 = new byte[width];
            byte[] cmdData2 = new byte[width];

            int length;
            for(length = 0; length < width; ++length) {

                for(int k = 0; k < 16; ++k) {
                    if (i + k < reSizeBitmap.getHeight() && reSizeBitmap.getPixel(length, i + k) == Color.rgb(0, 0, 0)) {
                        if (k % 2 == 0) {
                            cmdData1[length] = (byte)(cmdData1[length] | 1 << 7 - k / 2 % 8);
                        } else {
                            cmdData2[length] = (byte)(cmdData2[length] | 1 << 7 - k / 2 % 8);
                        }
                    }
                }
            }

            length = imageCommand.length;

            int j;
            for(j = 0; j < length; ++j) {
                data.add(imageCommand[j]);
                ++position;
            }

            length = cmdData1.length;

            for(j = 0; j < length; ++j) {
                data.add(cmdData1[j]);
                ++position;
            }

            length = lineFeed2.length;

            for(j = 0; j < length; ++j) {
                data.add(lineFeed2[j]);
                ++position;
            }

            length = buf.length;

            for(j = 0; j < length; ++j) {
                data.add(buf[j]);
                ++position;
            }

            length = cmdData2.length;

            for(j = 0; j < length; ++j) {
                data.add(cmdData2[j]);
                ++position;
            }

            length = lineFeed.length;

            for(j = 0; j < length; ++j) {
                data.add(lineFeed[j]);
                ++position;
            }
        }

        byte[] cmd = new byte[position];

        for(int i = 0; i < position; ++i) {
            cmd[i] = (Byte)data.get(i);
        }

        return cmd;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public int print(final Bitmap bitmap) {
        int result = 0;
        CommandBuilder commandBuilder = CommandBuilder.builder();

        int scaleHeight = bitmap.getHeight();
        int bitWidth = (bitmap.getWidth() + 7) / 8 * 8;
        int width = bitmap.getWidth();
        int[] data = new int[width * scaleHeight];

        commandBuilder
                .addCommand(COMMAND_PRINT_IMAGE)
                .addCommand(0)
                .addCommand(bitWidth / 8 % 256)
                .addCommand(bitWidth / 8 / 256)
                .addCommand(scaleHeight % 256)
                .addCommand(scaleHeight / 256);

        final Bitmap bitBlackWhite = PrinterUtils.bitmapToBlackWhite(bitmap);
        bitBlackWhite.getPixels(data, 0, width, 0, 0, width, scaleHeight);

        for(int h = 0; h < scaleHeight; ++h) {
            for(int w = 0; w < bitWidth; w += 8) {
                int value = 0;

                for(int i = 0; i < 8; ++i) {
                    int index = h * width + w + i;
                    if (w + i >= width) {
                        value = 0;
                    } else {
                        value |= PrinterUtils.px2Byte(data[index]) << 7 - i;
                    }
                }
                commandBuilder.addCommand((byte)value);
            }
        }

        byte[] builder = printImageForPin(bitmap);

//        byte[] builder = commandBuilder.build();

        Log.d(PRINTER_DEVICE_TAG, "Send Bitmap Print Command ... bytes: " + builder.length);

        //result = deviceConnection.bulkTransfer(endPointOut, builder, builder.length, getTimeout());

        Log.e("PDV-PINPAD", "\tresult: " + result);


        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setHeightDefaultBarCode() {
        return this.setHeightBarCode(HEIGHT_BARCODE_DEFAULT);
    }

    /** {@inheritDoc} */
    @Override
    public int setHeightBarCode(final int height) {
        int result = 0;
        Log.e("PDV-PINPAD", "Send Print Bar Code Command Height Buffer: (" + height + ")");

        final CommandBuilder commandBuilder = CommandBuilder.builder()
                .addCommand(COMMAND_BARCODE_HEADER);

        commandBuilder.addCommand(height >= 1 && height <= 255
                ? height
                : HEIGHT_BARCODE_DEFAULT);

        byte[] byteArray = commandBuilder.build();

        result = deviceConnection.bulkTransfer(this.endPointOut, byteArray, byteArray.length, getTimeout());

        Log.e("PDV-PINPAD", "\tresult: " + result);

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setWidthBarCode(final BARCODE_WIDTH_TYPE width) {
        int result = 0;

        Log.e("PDV-PINPAD", "Send: Set Width to Bar Code: (" + width.type + ")");
        final CommandBuilder commandBuilder = CommandBuilder.builder()
                .addCommand(COMMAND_SET_WIDTH_BARCODE);

        byte[] abCommand = commandBuilder.build();
        result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
        Log.e("PDV-PINPAD", "Set Width to Bar Code result: (" + result + ")");


        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int printerBarCodeCommand(final BARCODE_TYPE type, final String barCode) {
        return printerBarCodeCommand(type, barCode.getBytes());
    }

    /** {@inheritDoc} */
    @Override
    public int printerBarCodeCommand(final BARCODE_TYPE type, final byte[] buffer) {
        this.lineFeed();
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {

            Log.e("PDV-PINPAD", "Send Print Bar Code Command Buffer: (" + buffer + ")");

            if (type == null || buffer == null || buffer.length == 0){
                Log.e("PDV-PINPAD", "Send Print Bar Code Command Error, type or buffer is null.");
                return -1;
            }

            int bufferLen = buffer.length;
            outputStream.write(COMMAND_BARCODE_HEADER);
            outputStream.write(type.type);

            switch (type) {
                case UPC_A_2:
                case UPC_E_2:
                case EAN13_2:
                case EAN8_2:
                case CODE39_2:
                case ITF_2:
                case CODABAR_2:
                case CODE93:
                case CODE128:
                    outputStream.write(bufferLen);
                    break;
            }

            switch (type) {
                case UPC_A:
                case UPC_A_2:
                case UPC_E:
                case UPC_E_2:
                    if (11 <= bufferLen && bufferLen <= 12) {
                        if (checkByteArrayIsNumber(buffer)) {
                            outputStream.write(buffer);
                        }
                    }
                    break;
                case EAN13:
                case EAN13_2:
                    if (12 <= bufferLen && bufferLen <= 13) {
                        if (checkByteArrayIsNumber(buffer)) {
                            outputStream.write(buffer);
                        }
                    }
                    break;
                case EAN8:
                case EAN8_2:
                    if (7 <= bufferLen && bufferLen <= 8) {
                        if (checkByteArrayIsNumber(buffer)) {
                            outputStream.write(buffer);
                        }
                    }
                    break;
                case CODE39:
                case CODE39_2:
                    if (bufferLen <= 255) {
                        if (checkByteArrayIsCode39(buffer)) {
                            outputStream.write(buffer);
                        }
                    }
                    break;
                case ITF:
                case ITF_2:
                    if (bufferLen <= 255) {
                        if (checkByteArrayIsNumber(buffer)) {
                            outputStream.write(buffer);
                        }
                    }
                    break;
                case CODABAR:
                case CODABAR_2:
                    if (bufferLen <= 255) {
                        if (checkByteArrayIsCodaBar(buffer)) {
                            outputStream.write(buffer);
                        }
                    }
                    break;
                case CODE93:
                case CODE128:
                    if (bufferLen <= 255) {
                        outputStream.write(buffer);
                    }
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Add NULL Terminator to Output Command
        switch (type) {
            case UPC_A:
            case UPC_E:
            case EAN13:
            case EAN8:
            case CODE39:
            case ITF:
            case CODABAR:
                outputStream.write(0);
        }

        Log.e("PDV-PINPAD", "\tbCommand Bar Code: ");
        byte[] out = outputStream.toByteArray();
        int result = deviceConnection.bulkTransfer(this.endPointOut, out, out.length, getTimeout());
        Log.e("PDV-PINPAD", "\tresult: Bar Code" + result);

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setQrCodeModel() {
        return setQrCodeModel(QR_CODE_MODEL.MODEL_ONE);
    }

    /** {@inheritDoc} */
    @Override
    public int setQrCodeModel(final QR_CODE_MODEL qrCodeModel) {
        int result = 0;

        Log.e("PDV-PINPAD", "Select the QR Code Model: ");
        CommandBuilder commandBuilder = CommandBuilder.builder();

        byte[] bCommand = COMMAND_SET_QR_CODE_MODEL_SELECT;
        byte[] bChoose = qrCodeModel == QR_CODE_MODEL.MODEL_ONE
                                            ? COMMAND_SET_QR_CODE_MODEL_ONE
                                            : COMMAND_SET_QR_CODE_MODEL_TWO;
        int i = 0;
        for (final byte bPos : bChoose) {
            bCommand[ i++ ] |= bPos;
        }
        commandBuilder.addCommand(bCommand);
        byte[] abCommand = commandBuilder.build();

        result = deviceConnection.bulkTransfer(this.endPointOut, abCommand, abCommand.length, getTimeout());
        Log.e("PDV-PINPAD", "Set Qr Code Model result: (" + result + ")");

        return result;

    }

    /** {@inheritDoc}*/
    @Override
    public int setQrCodeSize() {
        return setQrCodeSize(6);
    }

    /** {@inheritDoc}*/
    @Override
    public int setQrCodeSize(final int size) {
        int result = 0;

        Log.e("PDV-PINPAD", "Set QR Code Size : ");
        byte[] bCommand = COMMAND_SET_QR_CODE_SIZE;
        bCommand[bCommand.length - 1] = (byte) size;

        result = deviceConnection.bulkTransfer(this.endPointOut, bCommand, bCommand.length, getTimeout());
        Log.e("PDV-PINPAD", "Set Qr Code Size result: (" + result + ")");

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int setQrCodeErrorCoorrectionLevel() {
        return this.setQrCodeErrorCoorrectionLevel(QR_CODE_LEVEL.LEVEL_L);
    }

    /** {@inheritDoc} */
    @Override
    public int setQrCodeErrorCoorrectionLevel(final QR_CODE_LEVEL level) {
        int result = 0;

        Log.e("PDV-PINPAD", "Set QR Code Error Correction Level: ");
        final CommandBuilder commandBuilder = CommandBuilder.builder();

        byte[] bCommand = COMMAND_SET_QR_CODE_ERROR_CORRECTION_LEVEL;
        bCommand[bCommand.length - 1] = (byte) level.type;

        result = deviceConnection.bulkTransfer(this.endPointOut, bCommand, bCommand.length, getTimeout());
        Log.e("PDV-PINPAD", "Set QR Code Error Correction Level result: (" + result + ")");

        return result;
    }

    /**{@inheritDoc}*/
    private int setQrCodeStorageData(final String data) {
        int result = 0;

        Log.e("PDV-PINPAD", "Set QR Code Storage Data: ");
        final CommandBuilder commandBuilder = CommandBuilder.builder();

        byte[] bCommand = COMMAND_STORE_QR_CODE_HEADER_DATA;
        bCommand[ 3 ] = (byte)(data.getBytes().length + 3 & 255);
        bCommand[ 4 ] = (byte)(data.getBytes().length + 3 >> 8 & 255);

        commandBuilder.build();
        result = deviceConnection.bulkTransfer(this.endPointOut, bCommand, bCommand.length, getTimeout());
        Log.e("PDV-PINPAD", "Set QR Code Storage Data result: (" + result + ")");

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int printQrCodeStorageData(final String data) {
        return printQrCodeStorageData(true, data);
    }

    /** {@inheritDoc} */
    @Override
    public int printQrCodeStorageData(final boolean configDefault, final String data) {
        int result = 0;

        if (configDefault) {
            result = setQrCodeModel();
            result += setQrCodeSize();
            result += setQrCodeErrorCoorrectionLevel();
        }

        if (0 != result || !configDefault) {
//            result += setQrCodeStorageData(data);

            Log.e("PDV-PINPAD", "Set QR Code Storage Data: ");
            final CommandBuilder commandBuilder = CommandBuilder.builder();

            byte[] bCommand = COMMAND_STORE_QR_CODE_HEADER_DATA;
            bCommand[ 3 ] = (byte)(data.getBytes().length + 3 & 255);
            bCommand[ 4 ] = (byte)(data.getBytes().length + 3 >> 8 & 255);

            byte[] bData = commandBuilder.addCommand(bCommand)
                            .addCommand(data.getBytes()).build();

            result = deviceConnection.bulkTransfer(this.endPointOut, bData, bData.length, getTimeout());

            if (0 != result) {
                result += deviceConnection.bulkTransfer(this.endPointOut, COMMAND_STORE_QR_CODE_PRINT_DATA, COMMAND_STORE_QR_CODE_PRINT_DATA.length, getTimeout());
                Log.e("PDV-PINPAD", "Set QR Code Storage Data result: (" + result + ")");
            }
        }
        return result;
    }

    /**
     * Command Aux to check byte is number
     *
     * @param buffer
     * @return
     */
    private boolean checkByteArrayIsNumber(byte[] buffer) {
        for (byte byteChar : buffer) {
            if (byteChar < 48 || byteChar > 57) {
                return false;
            }
        }
        return true;
    }

    private boolean checkByteArrayIsCode39(byte[] buffer) {
        int count = 0;

        if (buffer == null || buffer.length == 0) {
            return false;
        }

        for (byte byteChar : buffer) {
            if ((byteChar >= 48 && byteChar <= 57)
                    || (byteChar >=65 && byteChar <=90)
                    || (byteChar == 32)
                    || (byteChar == 36)
                    || (byteChar == 37)
                    || (byteChar == 43)
                    || (byteChar >= 45 && byteChar <= 47)) {
                count++;
            }
        }
        return count == buffer.length;
    }

    private boolean checkByteArrayIsCodaBar(byte[] buffer) {
        int count = 0;

        if (buffer == null || buffer.length == 0) {
            return false;
        }
        for (byte byteChar : buffer) {
            if ((byteChar >= 45 && byteChar <= 58)
                    || (byteChar == 36)
                    || (byteChar == 43)
                    || (byteChar >=65 && byteChar <=68)) {
                count++;
            }
        }

        return count == buffer.length;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return this.timeout;
    }

    @Override
    public int setCharset(int i) {
        byte[] byteArray = CommandBuilder.builder()
                .addCommand(COMMAND_CHARSET)
                .addCommand(i)
                .build();
        return deviceConnection.bulkTransfer(this.endPointOut, byteArray, byteArray.length, timeout);
    }

    @Override
    public int setCharset(CHARACTER_SET_TYPE charsetType) {
        byte[] byteArray = CommandBuilder.builder()
                .addCommand(COMMAND_CHARSET)
                .addCommand(charsetType.cod)
                .build();

        return deviceConnection.bulkTransfer(this.endPointOut, byteArray, byteArray.length, timeout);
    }

    @Override
    public int setCharacterCodeTable(int i) {
        byte[] byteArray = CommandBuilder.builder()
                .addCommand(COMMAND_CHARACTER_CODE_TABLE)
                .addCommand(i)
                .build();

        return deviceConnection.bulkTransfer(this.endPointOut, byteArray, byteArray.length, timeout);
    }
}
