/**
 * Manager Printer Class
 *
 * Start connection with all usb devices,
 * check printer and return Printer Found *
 */
package bt.com.itbam.devices.printers

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.*
import android.os.Parcelable
import android.util.Log

class PrinterFactory  {
    companion object : BroadcastReceiver() {
        private val PRODUCT_NAME = "JRSVC Printer"
        private var PRODUCT_ID : Int = 30016
        private var VENDOR_ID: Int = 1155

        val USB_SERVICE_NOT_STARTED = "Error on start Manager Service."
        val NOT_DEVICE_CONNECTED = "Not Device connected."

        private val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"
        private val PRINTER_FOUND = "Printer Found."
        private val PRINTER_NOT_FOUND = "Printer Not Found."
        private val MANAGER_PRINTER_TAG = "MANAGER PRINTER"

        private var context: Context? = null
        private var manager: UsbManager? = null
        private var printerDevice: Printer? = null
        var deviceName: String? = "printer"
            private set
        var lastError: String? = null
            private set

        fun getPrinter(context: Context, deviceName: String = "printer") : Printer? {
            this.context = context
            this.deviceName = deviceName
            this.manager = this.context!!.getSystemService(Context.USB_SERVICE) as UsbManager

            if (this.manager == null) {
                Log.d(MANAGER_PRINTER_TAG, USB_SERVICE_NOT_STARTED)
                this.lastError = USB_SERVICE_NOT_STARTED
                return null
            }

            val usbDevices = manager!!.deviceList
            if (usbDevices.size == 0) {
                Log.d(MANAGER_PRINTER_TAG, NOT_DEVICE_CONNECTED)
                this.lastError = NOT_DEVICE_CONNECTED
            } else {
                for (device in usbDevices.values) {
                    val productName = device.productName
                    val manufacturerName = device.manufacturerName
                    val serialNumber = device.serialNumber
                    val productId = device.productId
                    val vendorId = device.vendorId

                    Log.e(MANAGER_PRINTER_TAG, "\tproducName: ${productName};\n\tmanufacturerName:${manufacturerName}\n\tserialNumber:${serialNumber};\n\tproductId:${productId};\n\tvendorId:${vendorId};")

                    if (PRODUCT_ID == productId
                            && VENDOR_ID == vendorId) {
                        // check interfaces of devices
                        for (i in 0 until device.interfaceCount) {
                            val usbInterface = device.getInterface(i)
                            var endPointIn: UsbEndpoint? = null
                            var endPointOut: UsbEndpoint? = null

                            // check end points of devices
                            for (x in 0 until usbInterface.endpointCount) {
                                val endpoint = usbInterface.getEndpoint(x)

                                // Check Write End Point Permission
                                if (endpoint.type == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                                    requestPermission(device)
                                    // Get Device
                                    val deviceConnection = manager!!.openDevice(device)

                                    // Set the interface of connection to device
                                    deviceConnection.claimInterface(usbInterface, true)
                                    // check endpoint out and not instantiate
                                    if (endpoint.direction == UsbConstants.USB_DIR_OUT) {
                                        endPointOut = endpoint
                                    } else if (endpoint.direction == UsbConstants.USB_DIR_IN) {
                                        endPointIn = endpoint
                                    }

                                    if (null != endPointIn
                                            && null != endPointOut) {
                                        printerDevice = PrinterDevice(deviceConnection, endPointOut, endPointIn)
                                    }
                                }
                            }
                        }
                    }
                }

                lastError = if (printerDevice != null) {
                    // return the printer device found
                    Log.d(MANAGER_PRINTER_TAG, PRINTER_FOUND)
                    PRINTER_FOUND
                } else {
                    // no printer found
                    Log.d(MANAGER_PRINTER_TAG, PRINTER_NOT_FOUND)
                    PRINTER_NOT_FOUND
                }
            }

            return printerDevice
        }

        fun start() {

        }

        private fun requestPermission(device: UsbDevice) {
            val permissionIntent = PendingIntent.getBroadcast(this.context, 0, Intent(ACTION_USB_PERMISSION), 0)
            val filter = IntentFilter(ACTION_USB_PERMISSION)

            context!!.registerReceiver(this, filter)

            manager!!.requestPermission(device, permissionIntent)
        }

        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (ACTION_USB_PERMISSION == action) {
                synchronized(this) {
                    val device = intent.getParcelableExtra<Parcelable>(UsbManager.EXTRA_DEVICE) as UsbDevice

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        Log.d(MANAGER_PRINTER_TAG, "permission for device $device")
                    } else {
                        Log.d(MANAGER_PRINTER_TAG, "permission denied for device $device")
                        // TODO: Return callback on device permission denied
                    }
                }
            }
        }
    }
}
