package bt.com.itbam.devices.builder;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.CharsetEncoder;

public class CommandBuilder {
    private final ByteArrayOutputStream outputStream;

    private CommandBuilder() {
        outputStream = new ByteArrayOutputStream();
    }

    public static CommandBuilder builder() {
        return new CommandBuilder();
    }

    public CommandBuilder addCommand(byte[] byteCommand) {
        try {
            outputStream.write(byteCommand);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this;
    }

    public CommandBuilder addCommand(String strCommand) {
        try {
            if (strCommand != null) {
                outputStream.write(strCommand.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this;
    }

    public CommandBuilder addCommand(int iCommand) {
        outputStream.write(iCommand);
        return this;
    }

    public byte[] build() {
        byte b[] = new byte[0];
        try {
            b = outputStream.toString("UTF-8").getBytes();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return b;
    }
}
