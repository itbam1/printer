package bt.com.itbam.devices.printers;

import android.graphics.Bitmap;

public interface Printer {

    /**
     * Clear the data in the print buffer and reset the printer mode to the mode that was in effect when the power was turn on
     * @return
     */
    int initialize();

    /**
     * Return status
     *
     * @return
     */
    String getStatus();

    int horizontalTab();

    /**
     * Execute the line feed command in printer
     * @return
     */
    int lineFeed();

    /**
     * Execute the line feed command with n - lines
     *
     * @param lines
     * @return
     */
    int lineFeed(int lines);

    /**
     * Print and Carriage Return
     *
     * @return
     */
    int carriageReturn();

    /**
     * Print and return to standard mode in page mode
     *
     * @return
     */
    int setStandardMode();

    /**
     * Set print data in page mode
     *
     * @return
     */
    int setPageMode();

    /**
     * Configure the printer
     * @param changeFont
     * @param emphasized
     * @param doubleHeight
     * @param doubleWidth
     * @param underline
     * @return
     */
    int configurePrintMode(boolean changeFont, boolean emphasized, boolean doubleHeight, boolean doubleWidth, boolean underline);

    /**
     * Set character space default
     *
     * @return
     */
    int setCharacterSpaceDefault();

    /**
     * Set rigth-side character spacing
     * @param spaceValue
     * @return
     */
    int setCharacterSpace(int spaceValue);

    /**
     * Turn emphasized mode on/off
     *
     * @param emphasized
     * @return
     */
    int setEmphasized(boolean emphasized);

    int setDoubleStrike(boolean doubleStrike);

    /**
     * Set absolute print position
     *
     * @param posX
     * @param posY
     * @return
     */
    int setAbsolutePrintPosition(int posX, int posY);

    /**
     * Cancel print data in page mode
     * @return
     */
    int cancel();

    /**
     * Execute the papper sendCut in printer
     */
    void sendCut();

    /**
     * Printer buffer
     *
     * @param buffer
     * @return
     */
    int print(String buffer);

    /**
     * Set Underline in default dots mode
     * @return
     */
    int setUnderlineModeDefault();

    /**
     * Set Underline Mode to define in dotsMode
     *
     * @param dotsMode
     * @return
     */
    int setUnderlineMode(PrinterDevice.UNDERLINE_TYPE dotsMode);

    /**
     * Select Justification Mode
     * @param justificationType
     * @return
     */
    int setJustification(PrinterDevice.JUSTIFICATION_TYPE justificationType);

    /**
     * Turns on/off upside-down printing mode
     *
     * @param enabled
     * @return
     */
    int setUpsideDownPrintingMode(boolean enabled);

    /**
     * Set default line spacing
     *
     * @return
     */
    int setDefaultLineSpace();

    /**
     * Set line space
     * @param lineSpace
     * @return
     */
    int setLineSpace(int lineSpace);

    int setPrintDirection(int lineSpace);

    /**
     * Print the bitmap image
     *
     * @param bitmap
     * @return
     */
    int print(Bitmap bitmap);


    int printChar();

    /**
     * Select the font type
     *
     * @param fontType
     * @return
     */
    int selectFont(PrinterDevice.FONT_TYPE fontType);

    /**
     * Set the height default to barcode
     * @return
     */
    int setHeightDefaultBarCode();

    /**
     * Set the height the bar code
     *
     * @param height
     * @return
     */
    int setHeightBarCode(int height);

    /**
     * Set the width the bar code
     *
     * @param width
     * @return
     */
    int setWidthBarCode(PrinterDevice.BARCODE_WIDTH_TYPE width);

    /**
     * Print Bar Code By String value
     *
     * @param type
     * @param barCode
     * @return
     */
    int printerBarCodeCommand(PrinterDevice.BARCODE_TYPE type, String barCode);

    /**
     * Printer the Bar Code
     * @param type Specify the barcode type in
     *         UPC_A,
     *         UPC_E,
     *         EAN13,
     *         EAN8(3),
     *         CODE39,
     *         ITF,
     *         CODABAR,
     *         UPC_A_2,
     *         UPC_E_2,
     *         EAN13_2,
     *         EAN8_2,
     *         CODE39_2,
     *         ITF_2,
     *         CODABAR_2,
     *         CODE93,
     *         CODE128;
     * @param buffer The buffer value to Bar Code
     *
     * @return Size of Buffer writted
     */
    int printerBarCodeCommand(PrinterDevice.BARCODE_TYPE type, byte[] buffer);

    /**
     * Set Default Model Of Qr Code Print.
     *
     * @return
     */
    int setQrCodeModel();

    /**
     * Set Model Of Qr Code Print.
     *
     * @param qrCodeModel
     * @return
     */
    int setQrCodeModel(PrinterDevice.QR_CODE_MODEL qrCodeModel);

    /**
     * Set the QR Code default size of Module.
     *
     * @return
     */
    int setQrCodeSize();

    /**
     * Set the QR Code size of Module.
     *
     * @param size
     * @return
     */
    int setQrCodeSize(int size);

    /**
     * Set the Qr Code correction level default
     * @return
     */
    int setQrCodeErrorCoorrectionLevel();

    /**
     * Set the Qr Code Correction Level By Level Choose
     * @param level
     * @return
     */
    int setQrCodeErrorCoorrectionLevel(PrinterDevice.QR_CODE_LEVEL level);

    /**
     * Print QrCode Storage Data default
     *
     * @param data
     * @return
     */
    int printQrCodeStorageData(String data);

    /**
     * Print The QrCode Data Storage
     *
     * @return
     */
    int printQrCodeStorageData(boolean configDefault, String data);

    int setCharset(int i);

    int setCharset(PrinterDevice.CHARACTER_SET_TYPE charsetType);

    int setCharacterCodeTable(int i);
}
