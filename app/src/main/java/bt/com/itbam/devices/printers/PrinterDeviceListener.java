package bt.com.itbam.devices.printers;

public interface PrinterDeviceListener {

    /**
     * Return found printer on search callback
     *
     * @param printer
     */
    void onSearchPrinter(Printer printer);


    /**
     * Return not found printer callback
     */
    void onSearchPrinterNotFound();

}
